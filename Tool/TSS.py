# -- Time Statistics scripts --
#desinged to hold all GIS scripts and computations

#import libraries
import numpy as np
from osgeo import gdal
import os
import subprocess
import arcpy
from arcpy.sa import *
import matplotlib.pyplot as plt

# MSAVI creation function
def MSAVI_creation(file_location, entry_name, project_location):
    driver_name = 'GTiff'

    os.chdir(project_location)

    # Open the input image and get the red and nir bands.
    in_ds = gdal.Open(file_location)
    if not in_ds:
        raise IOError('Could not open raster')

    red_band = in_ds.GetRasterBand(2)
    nir_band = in_ds.GetRasterBand(3)

    # Get the dimensions.
    rows = in_ds.RasterYSize
    cols = in_ds.RasterXSize

    # Create an empty MSAVI raster.
    raster_name = entry_name + ".tif"
    out_ds = gdal.GetDriverByName(driver_name).Create(raster_name, cols, rows, 1, gdal.GDT_Float32)
    out_ds.SetProjection(in_ds.GetProjection())
    out_ds.SetGeoTransform(in_ds.GetGeoTransform())

    # Get the output band and set its NoData value to -99.
    out_band = out_ds.GetRasterBand(1)
    out_band.SetNoDataValue(-99)

    # Get the block size.
    block_cols, block_rows = red_band.GetBlockSize()

    # Loop through blocks of rows instead of one at a time.
    for y in range(0, rows, block_rows):

        # Figure out how many rows to read.
        if y + block_rows < rows:
            num_rows = block_rows
        else:
            num_rows = rows - y

        # Loop through blocks of columns.
        for x in range(0, cols, block_cols):

            # Figure out how many columns to read.
            if x + block_cols < cols:
                num_cols = block_cols
            else:
                num_cols = cols - x

            # Read the data using the numbers of rows and
            # columns just determined. Note the new x offset
            # and num_cols parameters.
            red = red_band.ReadAsArray(x, y, num_cols, num_rows)
            nir = nir_band.ReadAsArray(x, y, num_cols, num_rows)

            # Calculate MSAVI.
            #red = np.ma.masked_where(nir + red == 0, red) * 1.0
            msavi = (2 * nir + 1 - np.sqrt((2 * nir + 1)**2 - 8*(nir - red)))/2
            #msavi = msavi.filled(-99)

            # Write the output to the MSAVI band.
            out_band.WriteArray(msavi, x, y)

    # Calculate statistics.
    out_band.FlushCache()
    out_band.ComputeStatistics(False)

    # Close the datasets.
    del in_ds, out_ds

    
def clip(input_raster, input_shapefile, file_dir, output_raster):
    arcpy.env.workspace = file_dir
    arcpy.env.overwriteOutput = True
    
    Extent = arcpy.Describe(input_shapefile).extent
    SExtent = "%s %s %s %s" % (Extent.XMin, Extent.YMin, Extent.XMax, Extent.YMax)
    
    arcpy.Clip_management(input_raster, SExtent, output_raster, input_shapefile, "NoData", "ClippingGeometry")
    
def point_gen(numb, raster, file_dir):
    arcpy.env.workspace = file_dir
    arcpy.env.overwriteOutput = True
    arcpy.CheckOutExtension("Spatial")
    #reclassify raster
    Reclass = Reclassify(raster, "Value", RemapRange([[-1000,1000,1],["NoData","NoData","NoData"]]))   
    #raster to polygon
    outPolygons = "extent.shp"
    field = "VALUE"
    arcpy.RasterToPolygon_conversion(Reclass, outPolygons, "NO_SIMPLIFY", field)
    #generate random points
    arcpy.CreateRandomPoints_management(file_dir, "ranpoints", outPolygons, "", numb, "", "POINT", "")
    in_features = "ranpoints.shp"
    #clip points to raster
    arcpy.Clip_analysis(in_features, outPolygons, "samplepoints.shp")
    arcpy.Delete_management(outPolygons)
    arcpy.Delete_management(in_features)
    arcpy.CheckInExtension("Spatial")
    
#def diff_MSAVI_creation(first, second):   

# Timeplot creation function
def timeplot_creation(random_file, file_dir, t_entries):
    v = {}
    arcpy.env.workspace = file_dir
    arcpy.env.overwriteOutput = True
    
    inRasters = ["Entry" + str(i) + ".tif" for i in range(t_entries)]
    outTable = "MSAVI_Data"
    sampMethod = "NEAREST"
    arcpy.CheckOutExtension("Spatial")
    Sample(inRasters, random_file, outTable, sampMethod)
    arcpy.CheckInExtension("Spatial")
    

    x = np.array(range(t_entries))
    y = [arcpy.Raster ("Entry" + str(i) + ".tif").mean for i in range(t_entries)]
    plt.title('MSAVI time Values')
    plt.xlabel('Entry', fontsize=20)
    plt.ylabel('MSAVI value', fontsize= 20)
    plt.plot(x, y, 'bo')
    plt.savefig('time_plot.png')
    