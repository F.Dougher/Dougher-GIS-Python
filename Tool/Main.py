#tkinter enviroment
#this script will hold all tkinter code


#auxillary Library
import TSS

#import libraries
import Tkinter
from Tkinter import *
import tkMessageBox
import tkFileDialog

import os
import sys
import time
import webbrowser

import arcpy


#set init varibles
time1 = ''
last_update = int(time.strftime("%H%M"))

project_location = ''
study_location = ''
sample_location = ''

n_entries = 1

XT = 1

SAL = 2
RSP = 3

t = {}
e = {}
b = {}

#!!!!! load default options !!!!!!

#define special functions

class AutoScrollbar(Scrollbar):
    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)
    def pack(self, **kw):
        raise TclError, "cannot use pack with this widget"
    def place(self, **kw):
        raise TclError, "cannot use place with this widget"

def Cursor_track():
    cy = frame.winfo_pointery()
    c_abs_coord_y = frame.winfo_pointery() - frame.winfo_rooty()
    return c_abs_coord_y
    
def Button_near(cursor_pos, number_of):
    ret_ent = 'Entry'
    for i in range(number_of):
        assumed_ypos = (i * 75) + 25
        upp_y = assumed_ypos + 30
        low_y = assumed_ypos - 30
        if cursor_pos >= low_y and cursor_pos <= upp_y:
            ret_ent = ret_ent + str(i)
    return ret_ent

def E_File_Browse(number_of):
    entry = Button_near(Cursor_track(), number_of)
    filename = tkFileDialog.askopenfilename(initialdir = "/",title = "Select shapefile",filetypes = (("Image File","*.img"),("Tiff File","*.tif"),("all files","*.*")))
    e[entry].insert(0, filename)

def quit(event):
    #force quit
    print "you pressed control c"
    app.quit()
    sys.exit(0)
    
def Folder_Browse(btype):
    filename = tkFileDialog.askdirectory()
    if btype == "P":
        global project_location
        project_location = filename
        
def File_Browse(ftype):
    filename = tkFileDialog.askopenfilename(initialdir = "/",title = "Select shapefile",filetypes = (("shapefile","*.shp"),("all files","*.*")))
    if ftype == "S":
        global study_location
        study_location = filename
    if ftype == "R":
        global sample_location
        sample_location = filename
        
def O_save(check, radio, text):
    global SAL
    SAL = check
    global RSP
    RSP = radio
    global n_entries
    if text != n_entries:
        Create_Entries(int(n_entries), int(text))
        n_entries = text
            

#define new window functions

def Create_Entries(o_number_of, number_of):
    global frame
    frame.config(height = number_of * 75)
    if o_number_of != number_of:
        for X in range(o_number_of):
            var = "Entry" + str(X)
            t[var].destroy()
            e[var].destroy()
            b[var].destroy()
    
    for X in range(number_of):
        TitleT = "Entry " + str(X+1)
        var = "Entry" + str(X)
        ypos = X * 75

        t["Entry{}".format(X)] = Label(frame, text=TitleT, font=('times', 12))
        t[var].place(x=0,y=ypos)
        e["Entry{}".format(X)] = Entry(frame, width=40)
        e[var].place(x=0,y=ypos +25)
        b["Entry{}".format(X)] =Button(frame, text='Browse', width=5,command=lambda: E_File_Browse(number_of))
        b[var].place(x=250,y=ypos +25)

def c_options():
    
    Oapp = Toplevel()
    Oapp.title("Options")
    Oapp.geometry("300x300")
    
    # Study area (check box)
    Label(Oapp, text="Study Area Limiter", font=('times', 12)).pack(anchor=W)
    SAS = IntVar()
    SAS.set(SAL)
    C = Checkbutton(Oapp, text="Use Area limiting Shapefile", variable=SAS)
    C.pack(anchor=W)
   
    # sample points (radio button)
        # use users 
        # generate random
        # use entire raster
    Label(Oapp, text="Random Sample points", font=('times', 12)).pack(anchor=W)
    
    SP = IntVar()
    SP.set(RSP)
    R1 = Radiobutton(Oapp, text="User Specified", variable = SP, value=1)
    R1.pack(anchor=W)
    R2 = Radiobutton(Oapp, text="Generate", variable = SP, value=2)
    R2.pack(anchor=W)
    R3 = Radiobutton(Oapp, text="None", variable = SP, value=3)
    R3.pack(anchor=W)

    # number of time series (text box)
    Label(Oapp, text="Number of Entries", font=('times', 12)).pack(anchor=W)
    NEB = Entry(Oapp)
    NEB.pack(anchor=W)
    NEB.insert(0, n_entries)
    
    #save button
    SAVE_EX = Button(Oapp, text='Apply', width=5,command=lambda: O_save(SAS.get(), SP.get(), NEB.get()))
    SAVE_EX.pack(anchor=W)
    
    
def c_help():
    webbrowser.open("https://gitlab.com/F.Dougher/Dougher-GIS-Python/wikis/tool-info-and-help")
    
def main_options():
    global SALL
    global SAB
    global SA_EX
    global RSPL
    global RSP_EX
    global RSPB
    global RSPNB
    

    if SAL == 1:
        if SALL.winfo_exists() != 1:
            SALL = Label(app, text="Study Area file Location", font=('times', 12))
            SALL.place(x=0,y=375)
            
            SAB = Entry(app, width=40)
            SAB.place(x=0,y=400)
            SAB.insert(0, study_location)
            
            SA_EX = Button(app, text='Browse', width=5,command=lambda: File_Browse("S"))
            SA_EX.place(x=200,y=400)   
    else:
        if SALL.winfo_exists() == 1:
            SALL.destroy()
            SAB.destroy()
            SA_EX.destroy()
    
    if RSP == 1:
        if RSPL.winfo_exists() == 1:
            if RSP_EX.winfo_exists() != 1:
                RSPL.destroy()
                RSPB.destroy()
                RSPNB.destroy()
        if RSPL.winfo_exists() != 1:
            RSPL = Label(app, text="Sample points file Location", font=('times', 12))
            RSPL.place(x=0,y=425)
            
            RSPB = Entry(app, width=40)
            RSPB.place(x=0,y=450)
            RSPB.insert(0, sample_location)

            RSP_EX = Button(app, text='Browse', width=5,command=lambda: File_Browse("R"))
            RSP_EX.place(x=200,y=450)

    elif RSP == 2:
        if RSPL.winfo_exists() == 1:
            if RSP_EX.winfo_exists() == 1:
                RSPL.destroy()
                RSPB.destroy()
                RSP_EX.destroy()
        if RSPL.winfo_exists() != 1:
            RSPL = Label(app, text="Number of sample points", font=('times', 12))
            RSPL.place(x=0,y=425)

            RSPNB = Entry(app, width=10)
            RSPNB.place(x=0,y=450)
    else:
        if RSPL.winfo_exists() == 1:
            RSPL.destroy()
            RSPB.destroy()
            RSPNB.destroy()
            RSP_EX.destroy()
            
def popout_message(message):
    mapp = Toplevel()
    mapp.title("Alert")
    #mapp.geometry("200x100")
    
    # Study area (check box)
    Label(mapp, text=message, font=('times', 12)).pack()
    
    
# Main app run function

def run_tool():
    QA = 1
    #Prject Name
    if PNB.get() == '':
        QA = 0
    
    
    #project Location
    if project_location == '':
        QA = 0
    
    
    #sample point shapefile
    #if 
    if SAB.winfo_exists() == 1:
        if '.shp'  not in study_location:
            QA = 0
        
        
    if RSPB.winfo_exists() == 1:
        if '.shp'  not in sample_location:
            QA = 0
        
        
    if RSPNB.winfo_exists() == 1:
        if RSPNB.get() == '':
            QA = 0
        
        
    for i in range(int(n_entries)):
        var = "Entry" + str(i)
        if '.tif' not in str(e[var].get()):
            if '.img' not in str(e[var].get()): 
                QA = 0
                print str(e[var].get())
    
    # run checks to see if inputs are correct
    # run tool
    pro_folder = PNB.get() + '/'
    project_path = os.path.join(project_location, pro_folder)
    if QA == 0:
        popout_message("Not all requested forms were filled")
    elif os.path.isdir(project_path):
        popout_message("rename project")
    else:
        os.makedirs(project_path)
        
        for i in range(int(n_entries)):
            if SAB.winfo_exists() == 1:
                var = "Entry" + str(i)
                vart = "Entry" + str(i) + "f"
                TSS.MSAVI_creation(e[var].get(), vart, project_path)
                rastert = vart + '.tif'
                raster = var + '.tif'
                TSS.clip(rastert, study_location, project_path, raster)
                arcpy.Delete_management(rastert)
            else:
                var = "Entry" + str(i)
                TSS.MSAVI_creation(e[var].get(), var, project_path)
                raster = var + '.tif' 
        if RSP == 1:
            TSS.timeplot_creation(RSPB.get(), project_path, int(n_entries))
        if RSP == 2:
            TSS.point_gen(RSPNB.get(), "Entry0.tif", project_path)
            TSS.timeplot_creation("samplepoints.shp", project_path, int(n_entries))
        else:
            TSS.point_gen(100, "Entry0.tif", project_path)
            TSS.timeplot_creation("samplepoints.shp", project_path, int(n_entries))

        popout_message("tool complete")   
    # run tool
    #TSS.hello()

###################### APP BODY ############################################

    #set main app init    
app = Tk()
app.title("TimeStatistics")
app.geometry("700x800")

#scrollbar
vscrollbar = AutoScrollbar(app)
vscrollbar.grid(row=0, column=1, sticky=N+S)

canvas = Canvas(app,yscrollcommand=vscrollbar.set, width = 350, height = 600)
canvas.place(x=300,y=200)

vscrollbar.config(command=canvas.yview)

app.grid_rowconfigure(0, weight=1)
app.grid_columnconfigure(0, weight=1)

frame = Frame(canvas, width=700,height=600)

canvas.create_window(0, 0, anchor=NW, window=frame)

frame.update_idletasks()

canvas.config(scrollregion=canvas.bbox("all"))


#set tkinter init
SALL = Label(app, text="Study Area file Location", font=('times', 12))
SALL.place(x=0,y=375)
SALL.destroy()
SAB = Entry(app, width=40)
SAB.place(x=0,y=400)
SAB.insert(0, study_location)
SAB.destroy()          
SA_EX = Button(app, text='Browse', width=5,command=lambda: File_Browse("S"))
SA_EX.place(x=200,y=400)
SA_EX.destroy()
RSPL = Label(app, text="Study Area file Location", font=('times', 12))
RSPL.place(x=0,y=450)
RSPL.destroy()
RSPB = Entry(app, width=40)
RSPB.place(x=0,y=450)
RSPB.destroy()
RSPNB = Entry(app, width=10)
RSPNB.place(x=0,y=450)
RSPNB.destroy()
RSP_EX = Button(app, text='Browse', width=5,command=lambda: File_Browse("S"))
RSP_EX.place(x=200,y=475)
RSP_EX.destroy()

    #first line -- Title -- 
Label(app, text="Raster Time Series Statistics", font=('times', 20, 'bold')).place(x=0,y=0)

    #second line -- run - options - help --
execute_tool = Button(app, text='Run Tool', width=20,command=run_tool)
execute_tool.place(x=0,y=130)    
# !!!!!!!!!! consider moving execute_tool to bottom !!!!!!!!!!!!!!

w_options = Button(app, text='Options', width=20,command=c_options)
w_options.place(x=180,y=130)    

w_help = Button(app, text='Help', width=20,command=c_help)
w_help.place(x=360,y=130)

    #third Section (side left) --project name - save location--
    
#project name (text box)
Label(app, text="Project Name", font=('times', 12)).place(x=0,y=200)
PNB = Entry(app)
PNB.place(x=0,y=225)

# project location output (text box)
Label(app, text="Project Location Output", font=('times', 12)).place(x=0,y=250)

PLB = Entry(app, width=40)
PLB.place(x=0,y=275)
PLB.insert(0, project_location)

PL_EX = Button(app, text='Browse', width=5,command=lambda: Folder_Browse("P"))
PL_EX.place(x=200,y=275)

#imagery location (text box)
Label(app, text="--Extended Options--", font=('times', 12)).place(x=0,y=325)  
    #third Section (side Right) --varible inputs--
Create_Entries(n_entries, n_entries)


#tkinter loop
def Tasks_Update():
    global XT
    global SAB
    global RSPB
    global canvas
    if XT != 1:
        main_options()
        if SAB.winfo_exists() == 1:       
            if SAB.get() != study_location:
                print SAB.get()
                SAB.delete(0, END)
                SAB.insert(0, study_location)  
        if RSPB.winfo_exists() == 1:       
            if RSPB.get() != sample_location:
                RSPB.delete(0, END)
                RSPB.insert(0, sample_location)
    else:
        XT += 1
    if PLB.get() != project_location:
        PLB.delete(0, END)
        PLB.insert(0, project_location)

            
    
    frame.update_idletasks()
    canvas.config(scrollregion=canvas.bbox("all"))
    app.update_idletasks()
    app.after(1000, Tasks_Update)
    

Tasks_Update()
app.mainloop()